def py2(filename)
    begin
        system("python2 " + filename)
    rescue
        begin
            system("python2.9 " + filename)
        rescue 
            begin
                system("python2.8 " + filename)
            rescue 
                begin
                    system("python2.7 " + filename)
                rescue 
                    begin
                        system("python2.6 " + filename)
                    rescue  
                        begin
                            system("python2.5 " + filename)
                        rescue
                           begin
                               system("python2.4 " + filename)
                           rescue 
                               begin
                                   system("python2.3 " + filename)
                               rescue 
                                  begin
                                      system("python2.2 " + filename)
                                  rescue 
                                      begin
                                        system("python2.1 " + filename)  
                                      rescue
                                          begin
                                             system("python2.0 " + filename) 
                                          rescue 
                                             begin
                                                 system("python " + filename)
                                             rescue 
                                                  print "Python2 interpreter could not be located! Please enter path to interpreter. First thing to try is python. Please contact me for help, at gsbhasin84@gmail.com. path: "
                                                  interp = gets.chomp
                                                  system(interp + " " + filename)
                                             end
                                          end
                                      end
                                  end
                               end
                           end 
                        end
                    end
                end
            end
        end
    end
end