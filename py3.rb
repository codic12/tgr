def py3(filename)
    begin
        system("python3 " + filename)
    rescue
        begin
            system("python3.9 " + filename)
        rescue 
            begin
                system("python3.8 " + filename)
            rescue 
                begin
                    system("python3.7 " + filename)
                rescue 
                    begin
                        system("python3.6 " + filename)
                    rescue  
                        begin
                            system("python3.5 " + filename)
                        rescue
                           begin
                               system("python3.4 " + filename)
                           rescue 
                               begin
                                   system("python3.3 " + filename)
                               rescue 
                                  begin
                                      system("python3.2 " + filename)
                                  rescue 
                                      begin
                                        system("python3.1 " + filename)  
                                      rescue
                                          begin
                                             system("python3.0 " + filename) 
                                          rescue 
                                              print "Python3 interpreter could not be located! Please enter path to interpreter. First thing to try is python. Please contact me for help, at gsbhasin84@gmail.com. path: "
                                                  interp = gets.chomp
                                                  system(interp + " " + filename)
                                          end   
                                      end
                                  end
                               end
                           end 
                        end
                    end
                end
            end
        end
    end
end