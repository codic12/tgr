# TGR

TGR is a text adventure game platform in Ruby.
Currently, `se` (simple example, game ID 1) is the only available game, and is created as a showcase.

## HOW TO CREATE YOUR OWN GAME
Easy! First, code your game in Ruby (or python, see below...). A great guide to Ruby is https://learnrubythehardway.org/book/, and it's free. After that, fork this project (https://docs.gitlab.com/ee/gitlab-basics/fork-project.html). Using that fork, add your game logic to main.rb in a way. For example, put all your game code, normally, in game.rb (game.rb can be any name, but shouldn't have been already used by another file). game.rb can import other files. Look at this main.rb code (Not the whole thing, just a snippet):
```ruby
if doitem == 'startmission' or doitem == 'sm' or doitem == 'sg' or doitem == 'startgame'
        puts "This is the actual game. Remember, it's a WIP, so please report any bugs on GitLab."
        puts "Your choices are: [choices will keep updating, not shown in README]"
        print "Choose your mission number: "
        missionno = gets.chomp.to_i
        if missionno == 1
            system(rbp + " ./se.rb")
        end
```
In the 'your choices...' statement, just add yours. Then under the end block, copy paste the whole if block 
(```if missionno == 1...end```) but replace 1 with whatever your game's number is (the one you added to the your choices statement, it should be the next available number.) Additionally, replace se.rb with your own file name. Finally, create a Merge request. Click the left of MY repo (not your fork) and click New Merge Request. There, on the left choose your repo and the `master` branch should be autoselected. Add a title and description, and file it! I'll accept it after a review of the code, when possible. If you need help or find a bug, open an issue (click Issues on the left).

Or, code it in python 3/2. Here's for Python 3:
```ruby
if doitem == 'startmission' or doitem == 'sm' or doitem == 'sg' or doitem == 'startgame'
        puts "This is the actual game. Remember, it's a WIP, so please report any bugs on GitLab."
        puts "Your choices are: [choices will keep updating, nt shown in README]"
        print "Choose your mission number: "
        missionno = gets.chomp.to_i
        if missionno == 1
            system(rbp + " ./se.rb")
        end
        if missionno == 2
            py3('py3g.py')
        end
```
So replace py3g.py with your Python3 filename, 2 with your number (must use next in your choices are... statement), and then use the forking/merging process for Ruby.

Here's Python 2:
```ruby
if doitem == 'startmission' or doitem == 'sm' or doitem == 'sg' or doitem == 'startgame'
        puts "This is the actual game. Remember, it's a WIP, so please report any bugs on GitLab."
        puts "Your choices are: [choices will keep updating, nt shown in README]"
        print "Choose your mission number: "
        missionno = gets.chomp.to_i
        if missionno == 1
            system(rbp + " ./se.rb")
        end
        if missionno == 2
            py3('py3g.py')
        end
        if missionno == 3
            py2('py2g.py')
        end
```
Like Python 3, replace py2g.py with your Python2 filename, 3 with your number (must use next in your choices are... statement), and then use the forking/merging process for Ruby.