require 'colorize'
require_relative 'py3.rb'
require_relative 'py2.rb'
puts "Welcome to TGR, a text adventure game launcher written in Ruby. To get your own games in, please make a Merge Request as per the README"
rbp = RbConfig.ruby
loop do
   print "> " 
   doitem = gets.chomp.to_s.downcase.strip
   aval = ['help', 'sm', 'sg', 'startgame', 'statmission', 'clear', 'cls', 'exitme', 'exit']
   if aval.include?(doitem) != true
        puts "Command not found, try again. :(."
   end 
   if doitem == 'help' 
        puts %q{startmission, sm, startgame, sg - start a mission
exit, exitme - exit
clear - clear screen}
   end
    if doitem == 'clear' or doitem == 'cls'
        system("clear") || system("cls")
    end
	if doitem == 'exit' or doitem == 'exitme'
		puts "Welp... hope you had fun. Cya!"
        exit
    end
    if doitem == 'startmission' or doitem == 'sm' or doitem == 'sg' or doitem == 'startgame'
        puts "This is the actual game. Remember, it's a WIP, so please report any bugs on GitLab."
        puts "Your choices are: [1]Check out what my API can do! (example game for devs, not that stunning atm); [2]Python3 Example [3]Python2 Example"
        print "Choose your mission number: "
        missionno = gets.chomp.to_i
        if missionno == 1
            system(rbp + " ./se.rb")
        end
		if missionno == 2
			py3('py3g.py')
		end
        if missionno == 3
			py2('py2g.py')							
        end
		if missionno == 4
			# This is not gonna continue yet; lol
		end
    end  
end
