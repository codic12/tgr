def input(arg)
    print arg
    input = gets.chomp
    return input 
end

require 'fileutils'

if File.exist? ".futuregame/username.cfg"
    name = File.read(".futuregame/username.cfg")
else
    name = input("What's your name, new player? ")
    FileUtils.mkdir_p(".futuregame")
    FileUtils.touch(".futuregame/username.cfg")
    File.write(".futuregame/username.cfg", name)
end

puts("Hello there, #{name}!")
puts %Q{So, you have a couple missions to choose from:
[1] 2487 - stop your own code from taking over the world!}
print("Choose an option (number): ")
option = gets.chomp.to_i
if option == 1
    # This is the code for Mission One!
    puts("Mission 1. Background information: Your name is #{name}, and the year is 2487. AI is all over the world. It's become an integral part of human life. Python 18.4.8 was recently released, and a modern version of TensorFlow is what you're using. It's so easy to code in it!")
    puts %Q{Now that you got modern versions of Python and TensorFlow, let's make you an AI! 
OPTIONS:
[1] An AI program that can allocate resources on a computer easily!
}    
end