require 'colorize'
puts "This is a simple example of the TGR API.".red
puts "It isn't really a game, just a demo of what TGR can do. Well okay, what Ruby can do (although you are running it through TGR)".red
loop do 
    print "I AM A PARROT. Type something, and it will be echoed back: "
    parrot = gets.chomp.to_s
    exiter = parrot.strip.downcase
    if exiter == 'exitme'
        exit()
    end
    puts "YOU SAID, ".yellow + parrot.yellow
end